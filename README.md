jquery-soap-data-request-without-cors-error
===========================================

Disable Cross-domain Javascript security in Chrome for Development

This technique is very useful for writing Mobile apps and Ajax based apps.  Cross-domain Javascript security in all browsers will prevent you from making Ajax calls to other servers.   By passing the parameters to the browser, you can disable this temporarily.

For Windows:

1) Create a shortcut to Chrome on your desktop.  Right-click on the shortcut and choose Properties, then switch to “Shortcut” tab.

2) In the “Target” field, append the following:  –args –disable-web-security

For Mac, Open a terminal window and run this from command-line:
open ~/Applications/Google\ Chrome.app/ –args –disable-web-security
